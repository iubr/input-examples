## Dalton Input Examples

+ Relativistic NMR shielding

    [https://pubs.acs.org/doi/abs/10.1021/jp802238f](https://pubs.acs.org/doi/abs/10.1021/jp802238f)

+ Paramagnetic NMR shielding

    [https://pubs.acs.org/doi/abs/10.1021/jp902659s](https://pubs.acs.org/doi/abs/10.1021/jp902659s)

+ EPR parameters: hyperfine coupling and g-tensor

    [https://pubs.acs.org/doi/abs/10.1021/ct300606q](https://pubs.acs.org/doi/abs/10.1021/ct300606q)

+ Phosphorescence radiative rate constants

    [https://pubs.acs.org/doi/abs/10.1021/jp206279g](https://pubs.acs.org/doi/abs/10.1021/jp206279g)

    NOTE: MAY NOT WORK WITH LATEST DALTON!
